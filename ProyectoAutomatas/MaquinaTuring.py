# Maquina de Turing para comprobar palindromos
# Proyecto "Lenguajes Formales Automatas"

# Integrantes:
# Mosqueda Real Carlos Aaron
# Luis Francisco ROjas Diaz

# Fecha: Noviembre 26 de 2018

import sys
import string

class Turing_Machine:
    def __init__(self,state,write_head,tape_list):
        self.state = state
        self.write_head = write_head
        self.tape_list = tape_list

    def getState(self):
        return self.state

    def getHead(self):
        return self.write_head
    
    def getList(self):
        return self.tape_list

    # Tabla de reglas
    def updateMachine(self):
        # Estado inicial
        if (self.state == 'q1'):
            if (self.tape_list[self.write_head] != 0):
                ### ESTADO ### (p)
                char_read = self.tape_list[self.write_head]
                char_index = character_list.index(char_read)
                self.state = ''.join(['p',str(char_index)])
                ### ESCRIBIR ### (cero)
                self.tape_list[self.write_head] = 0
                ### MOVER ### (derecha)
                self.write_head += 1
            else:
                ### ESTADO ### (qy)
                self.state = 'qy'
                ### ESCRIBIR ### (cero, sin modificar)
                self.tape_list[self.write_head] = 0
                ### MOVER ### (derecha) (sin problema)
                self.write_head += 1
    
        elif (self.state.startswith('p')):
            if (self.tape_list[self.write_head]!=0):
                ### STATE ### (sin modificar)
                self.state = self.state
                ### WRITE ### (sin modificar)
                self.tape_list[self.write_head] = self.tape_list[self.write_head]
                ### MOVER ### (derecha)
                self.write_head += 1
            else:
                ### ESTADO ### (r)
                self.state = ''.join(['r',self.state[1:]])
                ### ESCRIBIR ### (cero, sin modificar)
                self.tape_list[self.write_head] = 0
                ### MOVER ### (izquierda)
                self.write_head -= 1
                    
        elif (self.state.startswith('r')):
            char_read = character_list[int(self.state[1:])]
            if (self.tape_list[self.write_head] != char_read and self.tape_list[self.write_head] != 0): # zero is needed for strings of odd length
                ### ESTADO ### (qn)
                self.state = 'qn'
                ### ESCRIBIR ###
                self.tape_list[self.write_head] = self.tape_list[self.write_head]
                ### MOVER ### (izquierda) (sin problema)
                self.write_head -= 1
            else:
                ### ESTADO ###
                self.state = 'q2'
                ### ESCRIBIR ### (cero)
                self.tape_list[self.write_head] = 0
                ### MOVER ### (izquierda)
                self.write_head -= 1
                
        elif (self.state == 'q2'):
            if (self.tape_list[self.write_head] != 0):
                ### ESTADO ### (sin modificar)
                self.state = 'q2'
                ### ESCRIBIR ### (sin modificar)
                self.tape_list[self.write_head] = self.tape_list[self.write_head]
                ### MOVER ### (izquierda)
                self.write_head -= 1
            else:
                ### ESTADO ### (q1)
                self.state = 'q1'
                ### ESCRIBIR ### (cero)
                self.tape_list[self.write_head] = 0
                ### MOVER ### (derecha)
                self.write_head += 1

# Definir conjunto de caracteres (caracteres permitidos para la comprobacion)
character_list = list(string.ascii_lowercase)
character_list.append(' ') # permitir espacios

# Cadena inicial

initial_string = raw_input('Ingresa una palabra para comprobar si es un palindromo: ')
print 'Comprobando...',initial_string
print '- - -'
initial_list = list(initial_string)

# Verificar que se usen solo caracteres validos
for i in initial_list:
    if i not in character_list:
        print 'Error! El caracter inicial >',i,'< no esta permitido en la lista!'
        sys.exit()

# Adjuntar lista
initial_list.append(0);

# Configurar la MT
i_write_head = 0
i_state = 'q1' # initial state
i_tape_list = initial_list

# Iniciar la clase
runMachine = Turing_Machine(i_state,i_write_head,i_tape_list)
print runMachine.getState(),runMachine.getHead(),runMachine.getList()

# Ejecutar la maquina
ctr=0
while runMachine.getState() != 'qy' and runMachine.getState() != 'qn' and ctr < 1000:
    runMachine.updateMachine();
    print runMachine.getState(),runMachine.getHead(),runMachine.getList()
    ctr += 1
print '- - -'

# Imprimir el resultado
if (runMachine.getState() == 'qy'):
    print initial_string,'*---ES UN PALINDROMO!---* Pasos:',ctr
else:
    print initial_string,'*---NO ES UN PALINDROMO!---* Pasos:',ctr

    

